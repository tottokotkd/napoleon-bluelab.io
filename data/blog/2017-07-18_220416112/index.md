---
title: ブログ開設・記事募集
icon:
createdDate: "2017-07-18T22:04:16+09:00"
updatedDate: "2017-07-18T22:04:16+09:00"
author: tottokotkd
tags:
  - notice
draft: false
desc: ブログの製造・開設・記事募集のお知らせ
---

napoleon.blue プロジェクトからのお知らせです。

2017.7 アップデートの一部として、本サイトにブログを製造・設置しました。実装の技術的詳細については、[こちらの記事](../2017-07-18_223834195/)をご覧ください。

## 記事募集

このブログでは、今後しばらくは開発ロードマップの説明や本サイトの技術的な解説記事が多くなると思います (ご承知の通り、執筆に当たるスタッフはエンジニアばかりです) が、閲覧者の皆さまからの様々なメッセージも掲載していきたいと考えています。

そこで早速ですが、本ブログに記事をお寄せくださる方を募集したいと思います！ 

ユニットとしてのブルナポやそのメンバーについて、あるいはライブの感想から総選挙の感想まで、筆の赴くままに記事をお寄せください。

## 投稿方法

[プロジェクトのSlack](https://join.slack.com/napoleon-blue/shared_invite/MjAxMTY1NDI4NjMwLTE0OTc5NDM0NzctZDgwNjY0MTVkMA)にご参加いただき、投稿希望の旨をお伝えいただくと確実です。開発メンバーの[Twitterアカウント](https://twitter.com/seymour_glasses)あるいは[メールアドレス](tottokotkd@gmail.com)までご連絡いただくこともできますが、諸般の事情により帰宅しておらず、応答できない場合があります。ご了承ください。

エンジニアの方は、上記の方法で投稿していただくか、GitLabリポジトリーに直接マージリクエストをお送りください。

なお、本プロジェクトはApache v2ライセンスが適用される予定ですが、ブログ記事等はその対象外となっております。投稿した記事にオープンソースライセンスが適用されることはありませんので、ご安心ください。

皆さまの投稿をお待ちしています！
