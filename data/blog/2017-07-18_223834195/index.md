---
title: React, Webpack, GraphQLを用いる静的サイトジェネレーター “Gatsby” で遊ぼう
icon: gatsby.png
createdDate: "2017-07-18T22:38:34+09:00"
updatedDate: "2017-07-18T22:38:34+09:00"
author: tottokotkd
tags:
  - gatsby
draft: false
desc: 知る人ぞ知る… というか誰も知らない静的サイトジェネレーター Gatsby 紹介記事。
---

## 静的サイトの強み

サイト配信のために使える予算はない (or 使いたくない) けど、Wordpressみたいなサイトを持ちたい。アクセスが沢山あるかもしれないけど、サーバーとかダウンロードとか難しいことはなるべく考えたくない。そんなとき活躍するのが静的サイトジェネレーターです。

あまり巷で流行っている様子はないものの、ITエンジニアは割とユーザーが多いような気がします。それは何故かというと、やはりGitHubで無料配信できるからなんでしょう。

無料ブログが欲しいだけなら「はてなブログ」を使えば済むわけですが、Gatsbyを使えばどのようなサイトでも生成することができ、極めて安上がりに配信できます。実際、このサイトもすべてGatsbyで製造されています。

## Gatsbyとは

Gatsbyを一言で表現するなら、Reactによるサーバーサイドレンダリングを適切に一体化するフレームワークです。一定のディレクトリ構成に従ってReactコンポーネントを配置すれば、それだけでサイトをビルドできます。

静的サイトジェネレーターにおいてReactが使えるということは、ジェネレーター独自のDSL (とも呼べないような何か) を覚える必要がないということです。ツール毎に分断されたDSLに比べて、Reactには膨大な技術的蓄積と応用範囲があり、初心者がとっつきやすいドキュメントも整備されています。GatsbyがReactを全面的に採用していることは大きな利点といえます。

そして新たに公開されたバージョン1.0は、ここに重大な (同時にあり得ないほど破壊的な) 新機能を加えました。このバージョンにおいて、GraphQLによるファイルリソース利用とルーティングのプログラム化が実現したのです。この2つの新機能の応用範囲を理解すれば、Gatsbyを利用するモチベーションはますます高まるでしょう。

簡単に、1つずつ見ていきましょう。

### React 

この節を読む前に、本サイトのページリンクを使ってもらいたいと思います。このブログ記事から、タグページやトップページへ移動して、そこからまた戻ってみてください。そうすると、ページ間を移動するときにロードが発生しないことに気がつくでしょう (もしあからさまなロードが発生するなら、それはバグであるか、JavaScriptが無効になっています)。

React化の大きな利点は、サイト全体が1つのアプリケーションとなり、最初のロードさえ終わってしまえば無様ななロード画面を晒さずに済むことです。もちろんGatsbyはサーバーサイドレンダリングを適切に埋め込むよう設定されていますから、最初のロードが終わるのを待つ間もコンテンツは表示されますし、仮にユーザーがJavaScriptを無効にしていても最低限の内容は出すことができます。

### webpackJsonp

Reactアプリを製造した経験のあるエンジニアなら「サイト全体を1つのアプリにすると、バンドルファイルが巨大になりすぎる」と懸念するかもしれません。しかし、Gatsbyなら問題ありません。

Gatsbyにビルドを任せておけば、Reactコンポーネントやブログ記事などのデータソースを細かく分割し、webpackJsonpの管理下に置いてくれます。たとえ大量のページがあっても、あるページを描写するために必要なデータだけをロードすればいいのです。

### GraphQL

いよいよGraphQLです！

Webpackによってページのダウンロード量を最小化するには、そのページを描写するために何が必要であるか、正確に知る必要があります。もし `articles.json` などという巨大なファイルに全てのデータを詰め込んでしまったら、結局のところ、一つの記事を表示するために全ての記事をダウンロードすることになります。

Gatsbyでは、Reactコンポーネントを書いたjsファイルにGraphQLクエリーを書くことで、この問題を巧妙に解決しています。そのページが必要とするデータは全て、クエリー経由で取得することができるのです。

つまり、こういう流れになります。

1. Reactコンポーネントを作る
2. 欲しいデータをGraphQLで指定する
3. GatsbyがデータをJsonp管理下に置き、Reactコンポーネントのprops.dataに詰めてくれる

例えば、ブログのトップページに「原稿のうち、下書きフラグがないものを、新しい方から5件表示したい」と思ったとします。それをクエリで表現すると、こうなるでしょう。

```js
export const pageQuery = graphql`
query BlogPostsForTopPage {
  allMarkdownRemark(
    limit: 5,
    sort: {fields: [frontmatter___createdDate], order: ASC},
    filter: {
      frontmatter: {draft: {ne: true}}
      fields: {slug: { regex: "^/blog/" }}
    }
  ) {
    edges {
      node {
        fields {
          slug
        }
        html
        frontmatter {
          title
          tags
          createdDate
          author {
            name
            avatar {
              sharp: childImageSharp {
                data: responsiveResolution(width: 80, height: 80, quality: 100) {
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
}
`;
```

ソースファイルにこう書いておけば、ただそれだけで、指定されたデータがpropsに与えられます。面倒なことはGatsbyが全てやってくれるのです。素晴らしいですね。

### 柔軟なルーティング

GatsbyのGraphQLインターフェイスは素晴らしい機能ですが、まだ続きがあります。ページ毎のクエリには引数を設定でき、その引数の値はページ生成時に設定できるのです。どういうことでしょうか？

例えば、あるタグのついた記事すべてのデータが欲しいとします。React側が要求するクエリはこんな風になるでしょう。

```js
export const pageQuery = graphql`
query BlogPostsByTag($tag: String!, $skip: Int!) {
  result: allMarkdownRemark(
    limit: 10
    skip: $skip
    sort: { fields: [frontmatter___createdDate], order: DESC }
    filter: {
      fields: {
        slug: { regex: "^/blog/" }
      }
      frontmatter: {
        tags: { in: [$tag] },
        draft: { ne: true }
      }
    }
  ) {
    totalCount
    posts: edges {
      node {
        excerpt
        timeToRead
        fields {
          slug
        }
        frontmatter {
          tags
          title
          updatedDate
          createdDate
          author {
            id
            name
            avatar {
              sharp: childImageSharp {
                data: responsiveResolution(width: 80, height: 80, quality: 100) {
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
}
`;
```

`$tag` という変数に注目してください。ブログのトップページと違って、タグは一種類ではありません。いちいち人力でタグ別ページを作っていたら日が暮れてしまいます。そこで、全ての原稿からタグを抽出し、機械的に `$tag` を設定してタグページを自動生成していくのです。

そう、抽出とはGraphQLによるデータ取得です。つまりGatsbyでは「どこにいくつのページを作るか」を決定するためにGraphQLシステムを使えるのです。

この機能を使えば、以下のような処理が可能になるでしょう。

* `allMarkdownRemark` データを利用し、個別の記事ページと、ページング付き一覧ページを作る
* `allAuthorJson` データを組み合わせ、寄稿数トップ3の著者の個別ページを作る
* `allIdolJson` データを利用し、`idols.json` のデータから全アイドルのページを作る (プラグインさえあれば、独自のJSONも検索できます)

## 現状と展望

正直なところ、Gatsby v1は公開されたばかりであり、バグも多いです。このサイトをバージョンアップするときも「あるファイルをTypeScriptの文法で書くと絶対に起動しない」とか、「初回ビルドは必ず失敗する」などという理解に苦しむ問題が多発しました (こうした課題は未だに直っていません)。こういった部分は割り切る必要があります。

そうしたマイナス点を差し引いてもなお、Gatsbyは取り組む価値のあるツールだと思います。Webpack, React, GraphQLをこの水準で統合するフレームワークは、今後もそう多く出ることはないでしょう。プラグインによる機能追加が可能であり、現時点でも「Wordpress APIからデータを引っこ抜いてGatsbyのデータソースにするプラグイン」「画像ファイル名からsrcsetを自動生成するプラグイン」といった興味深い機能が提供されています。
