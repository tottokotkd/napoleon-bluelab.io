---
title: 5thライブに「ブルーナポレオン」登場！
icon:
createdDate: "2017-08-14T22:04:59+09:00"
updatedDate: "2017-08-14T22:04:59+09:00"
author: project
tags:
  - event
draft: false
desc: 5人組ユニット「ブルーナポレオン」名義で “Nation Blue” を歌うパフォーマンスを披露しました！
---

今井麻夏 (佐々木千枝役)、東山奈央 (川島瑞樹役)、長島光那 (上条春菜役) の3名が、[THE IDOLM@STER CINDERELLA GIRLS 5thLIVE TOUR Serendipity Parade!!!](http://idolmaster.jp/event/cinderella5th/information.php) さいたまスーパーアリーナ公演において、5人組ユニット「ブルーナポレオン」名義で “Nation Blue” を歌うパフォーマンスを披露しました！

ライブに出演され、参加され、観覧された全ての方々、本当にお疲れさまでした。今後もブルナポをよろしくお願いします。
