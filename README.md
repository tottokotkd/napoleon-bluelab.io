# Blue Napoleon (unofficial)

ブルーナポレオンのために最新のWebフロント技術を結集する napoleon.blue プロジェクトへようこそ！

## napoleon.blue とは

**「シンデレラガールズ」における最古参ユニット「ブルーナポレオン」にアイドルとしての公式サイトが作られるなら、こんなサイトが欲しい** というブルナポPの妄執を実現する非公式プロジェクトです。プロジェクトの成果物は[napoleon.blue](https://napoleon.blue/) にホストされます。

## プロジェクトに参加する

誰でもこのプロジェクトに参加することができます。ブルナポが好きな人、モダンなWebフロント技術を使ってみたい人、雑談をしたい人、全員を歓迎します。このプロジェクトは、あなたのテキストやイラスト、アイデア、コード、そして感想を必要としています！

### Slack

プロジェクトに関するやりとりは [Slack](https://join.slack.com/napoleon-blue/shared_invite/MjAxMTY1NDI4NjMwLTE0OTc5NDM0NzctZDgwNjY0MTVkMA) において行われています。誰でも参加できます。

## napoleon.blue を支えるテクノロジー
napoleon.blue は React ベースの静的サイトジェネレーター [gatsby](https://github.com/gatsbyjs/gatsby) を全面的に採用しています。新規コードは原則として TypeScript / SCSS により記述されており、 Webpack におるホットリロードや Babel を用いたIE対応などのテクニックも採り入れています。

本プロジェクトは、こうしたテクノロジーを実践的に学ぶ場として機能しています。もしあなたが HTML や CSS の学習を済ませており、もう一歩進んだサイトを構築してみたいと思うなら、 napoleon.blue プロジェクトのソースから多くのことを学べるでしょう。

このプロジェクトのGitterでは、ソースコードに関する質問はもちろん、Webテクノロジーの学習方法や低コストな配信方法などの一般的な相談をすることもできます。

### ローカルで起動するには

napoleon.blue の開発に参加したり、その動作を確認したりするためには、以下の手順に従って開発環境を構築してください。

1. [node.js](https://nodejs.org/ja/), [yarn](https://yarnpkg.com/lang/en/) をインストール
2. `windows-build-tools` をインストール (Windowsのみ。後述)
3. [git](https://git-scm.com/) をインストール
4. `git clone https://gitlab.com/napoleon-blue/napoleon-blue.gitlab.io.git`
5. `cd napoleon-blue.gitlab.io.git`
6. `yarn install`
7. `npm run develop`

#### windows-build-tools

gatsbyを正しくインストールするためには、あらかじめ `windows-build-tools` を用意する必要があります。

1. 管理者権限でコマンドプロンプト or PowerShellを起動
2. `npm install --global windows-build-tools` を実行し、ツールをインストール
3. そのまま作業を継続せず、必ずウィンドウを閉じる

## Let‘s encrypt更新はお早めに
certbot certonly --manual --email tottokotkd@me.com -d napoleon.blue

- Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/napoleon.blue/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/napoleon.blue/privkey.pem
   Your cert will expire on 2017-11-09. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
