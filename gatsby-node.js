/**
 * Created by tottokotkd on 27/05/2017.
 */

const path = require('path');
const slash = require('slash');
const {kebabCase, uniq, get, range, map} = require('lodash');

// Create slugs for files.
// Slug will used for blog page path.
exports.onCreateNode = ({node, boundActionCreators, getNode}) => {
    const {createNodeField} = boundActionCreators;
    let slug;
    switch (node.internal.type) {
        case `MarkdownRemark`:
            const fileNode = getNode(node.parent);
            const [basePath, name] = fileNode.relativePath.split('/');
            slug = `/${basePath}/${name}/`;
            break;
    }
    if (slug === '/about/site/') {
        createNodeField({node, name: `slug`, value: '/about/'});
    } else if (slug) {
        createNodeField({node, name: `slug`, value: slug});
    }
};

// Implement the Gatsby API `createPages`.
// This is called after the Gatsby bootstrap is finished
// so you have access to any information necessary to
// programatically create pages.
exports.createPages = ({graphql, boundActionCreators}) => {
    const {createPage} = boundActionCreators;

    return new Promise((resolve, reject) => {
        const blogPost = path.resolve("src/templates/blog-post.tsx");
        const blogPosts = path.resolve("src/templates/blog-posts.tsx");
        const tagPosts = path.resolve("src/templates/tag-posts.tsx");
        const newsPost = path.resolve("src/templates/news-post.tsx");
        const newsPosts = path.resolve("src/templates/news-posts.tsx");
        const aboutPage = path.resolve("src/templates/about.tsx");

        const createPages = (posts, component) => {
            for (const post of posts) {
                const {slug} = post.fields;
                // Create blog posts pages.
                createPage({
                    path: slug,
                    component: slash(component),
                    context: {slug},
                })
            }
        };

        const createPaging = (pageCount, path, component, context = {}) => {
            for (const index of range(pageCount)){
                context.skip = index * 10;
                if (!index) {
                    createPage({
                        path, context,
                        component: slash(component),
                    });
                }

                createPage({
                    path: `${path}${index + 1}/`,
                    component: slash(component),
                    context: context
                });
            }
        };

        // news page
        resolve(
            // 公開対象となる記事のみ取得
            graphql(
                `{
                    posts: allMarkdownRemark(
                      filter: {
                        fields: {slug: { glob: "/news/*/" }}
                        frontmatter: {draft: {ne: true}}
                      }
                  ) {
                    totalCount
                    edges {
                      node {
                        fields {
                          slug
                        }
                      }
                    }
                  }
                }`
            ).then(result => {
                if (result.errors) {
                    console.log(result.errors);
                    reject(result.errors)
                } else if (!result.data.posts) {
                    return
                }

                const posts = result.data.posts ? map(result.data.posts.edges, p => p.node) : [];

                // post page
                createPages(posts, newsPost);

                // posts paging
                // 10ずつページング
                const pageCount = Math.ceil(result.data.posts.totalCount / 10);
                createPaging(pageCount, '/news/', newsPosts)
            })
        );

        // blog posts
        resolve(
            // 公開対象となる記事のみ取得
            graphql(
                `{
                    posts: allMarkdownRemark(
                      filter: {
                        fields: {slug: { regex: "^/blog/\\\\d+-\\\\d+-\\\\d+_\\\\d+/" }}
                        frontmatter: {draft: {ne: true}}
                      }
                  ) {
                    totalCount
                    edges {
                      node {
                        fields {
                          slug
                        }
                        frontmatter {
                          tags
                        }
                      }
                    }
                  }
                }`
            ).then(result => {
                if (result.errors) {
                    console.log(result.errors);
                    reject(result.errors)
                } else if (!result.data.posts) {
                    // 記事が1つもない場合も、最初のページだけは作る
                    createPaging(1, '/', blogPosts);
                    return
                }


                const posts = map(result.data.posts.edges, p => p.node);

                // post page
                createPages(posts, blogPost);

                // posts paging
                // 10ずつページング
                const pageCount = Math.ceil(result.data.posts.totalCount / 10);
                createPaging(pageCount, '/blog/', blogPosts);
            })
        );

        // tag
        resolve(
            // 公開対象となる記事のみ取得
            graphql(
                `{
                  posts: allMarkdownRemark(
                    filter: {
                      frontmatter: { draft: { ne: true } }
                    }
                  ) {
                    edges {
                      node {
                        frontmatter {
                          tags
                        }
                      }
                    }
                  }
                }`
            ).then(result => {
                if (result.errors) {
                    console.log(result.errors);
                    reject(result.errors)
                } else if (!result.data.posts) {
                    return
                }

                const posts = map(result.data.posts.edges, p => p.node);

                // tags pages
                const tagPageCounts = {};
                for (const post of posts) {
                    const tags = get(post, 'frontmatter.tags') || [];
                    tags.forEach(t => tagPageCounts[t] = (tagPageCounts[t] || 0) + 1)
                }
                for (const tag in tagPageCounts) {
                    // 10ずつページング
                    const pageCount = Math.ceil(tagPageCounts[tag] / 10);
                    createPaging(pageCount, `/tags/${kebabCase(tag)}/`, tagPosts, {tag})
                }
            })
        );

        // about page
        createPage({
            path: '/about/',
            component: slash(aboutPage),
            context: {slug: '/about/'},
        });
    })
};
