/**
 * Created by tottokotkd on 2017/07/06.
 */

import * as React from "react";
import {Helmet} from "react-helmet";
import Layout, {MenuItem} from "../../layouts/content";

import PageRow from "../Row";
import ArticleList, {IProps as ArticleProps} from "./ListItem";

interface IProps {
    menu: MenuItem;
    title?: string;
    posts: ArticleProps[];
}

export default class extends React.Component<IProps, {}> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {menu, title, posts} = this.props;
        return (
            <Layout item={menu}>
                <Helmet title={title} meta={[
                    {name: "description", content: title},
                    {name: "twitter:description", content: title},
                ]} />
                <div className="container">
                    {title ? <PageRow className="page-title" right={<h1>{title}</h1>} /> : null}
                    {posts.map((p, i) => <ArticleList key={i} {...p} />)}
                </div>
            </Layout>
        );
    }
}
