/**
 * Created by tottokotkd on 2017/07/14.
 */

import Link from "gatsby-link";
import * as React from "react";

import PageRow from "../Row";

export interface IProps {
    title: string;
    link?: string;
    icon?: {
        src: string;
        srcSet: string;
    };
}

export default class extends React.Component<IProps, {}> {
    public render() {
        const {title, link, icon} = this.props;
        const img = icon ? <img className="icon-image" src={icon.src} srcSet={icon.srcSet} alt={title}/> : null;

        return <PageRow
            className="title" left={img}
            right={
                link ? <Link to={link}><h1 className="m-0">{title}</h1></Link> : <h1 className="m-0">{title}</h1>
            }
        />;

    }
}
