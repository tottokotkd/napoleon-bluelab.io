/**
 * Created by tottokotkd on 2017/07/14.
 */

import * as GithubSlugger from "github-slugger";
import * as React from "react";

import PageRow from "../Row";

export interface IProps {
    headings?: [{value: string, depth: number}];
    html: string;
}

export default class extends React.Component<IProps, {}> {
    public render() {
        const {html, headings} = this.props;

        const slugger = new GithubSlugger();
        const headingLinks = headings ? headings.map((heading, index) => {
            return (
                <li key={index} className="nav-item">
                    <a className="nav-link px-0" href={`#${slugger.slug(heading.value)}`}>
                        {heading.value}
                    </a>
                </li>
            );
        }) : null;

        return <PageRow
            className="content"
            left={
                <nav>
                    <ul className="nav flex-column">
                        {headingLinks}
                    </ul>
                </nav>
            }
            right={
                <div dangerouslySetInnerHTML={{ __html: html }} />
            }
        />;
    }
}
