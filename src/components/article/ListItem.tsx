/**
 * Created by tottokotkd on 2017/07/14.
 */

import * as React from "react";

import PageRow from "../Row";
import ListTags from "./Tags";
import ListTimestamp from "./Timestamp";
import ListTitle from "./Title";

import "../../css/article/list.scss";

export interface IProps {
    fields: {
        slug: string;
    };
    excerpt: string;
    timeToRead: number;
    frontmatter: {
        title: string;
        tags?: string[];
        desc?: string;
        createdDate: string;
        updatedDate?: string;
        icon?: { sharp: { data: {
            src: string;
            srcSet: string;
        } } };
        author: {
            icon?: { sharp: { data: {
                src: string;
                srcSet: string;
            } } };
        };
    };
}

export default class extends React.Component<IProps, {}> {
    public render() {
        const {fields, excerpt, timeToRead, frontmatter} = this.props;
        const {title, tags, createdDate, updatedDate} = frontmatter;
        const icon = frontmatter.icon ? frontmatter.icon.sharp.data :
            (frontmatter.author.icon ? frontmatter.author.icon.sharp.data : undefined);
        const desc = frontmatter.desc || excerpt;
        return (
            <div className="list-item">
                <ListTitle title={title} link={fields.slug} icon={icon} />
                <PageRow className="desc" right={desc}/>
                <ListTimestamp createdDate={createdDate} updatedDate={updatedDate} />
                <ListTags tags={tags || []} />
            </div>
        );

    }
}
