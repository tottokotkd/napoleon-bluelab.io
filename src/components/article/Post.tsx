/**
 * Created by tottokotkd on 2017/06/18.
 */

import * as React from "react";
import {Helmet} from "react-helmet";

import Author from "./Author";
import Content from "./Content";
import Tags from "./Tags";
import Timestamp from "./Timestamp";
import Title from "./Title";

import "../../css/article/post.scss";

export interface IProps {
    html: string;
    excerpt: string;
    headings: [{value: string, depth: number}];
    frontmatter: {
        title: string;
        tags?: string[];
        desc?: string;
        icon?: { sharp: { data: {
            src: string;
            srcSet: string;
        } } };
        createdDate: string;
        updatedDate?: string;
        author: {
            id: string;
            name: string;
            bio: string;
            twitter?: string;
            avatar?: { sharp: { data: {
                src: string;
                srcSet: string;
            } } }
            icon?: { sharp: { data: {
                src: string;
                srcSet: string;
            } } };
        };
    };
}

export default class extends React.Component<IProps, {}> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {html, headings, excerpt, frontmatter} = this.props;
        const {title, author, updatedDate, createdDate, tags, desc} = frontmatter;
        const icon = (frontmatter.icon ? frontmatter.icon.sharp.data : undefined)
            || (author.icon ? author.icon.sharp.data : undefined);
        const avatar = author.avatar ? author.avatar.sharp.data : {src: "", srcSet: ""};
        const pageDesc = desc || excerpt;

        return (
            <div>
                <Helmet title={title} meta={[
                    {name: "description", content: pageDesc},
                    {name: "twitter:description", content: pageDesc},
                ]} />

                <article className="container post">
                    <Title title={title} icon={icon} />
                    <Timestamp createdDate={createdDate} updatedDate={updatedDate} />
                    {tags ? <Tags tags={tags} /> : null}
                    <Content headings={headings} html={html} />
                    <Author name={author.name} bio={author.bio} icon={avatar} />
                    {this.props.children}
                </article>
            </div>
        );
    }
}
