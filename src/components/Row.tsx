/**
 * Created by tottokotkd on 2017/07/14.
 */

import * as React from "react";

export interface IProps extends React.HTMLProps<HTMLDivElement> {
    left?: React.ReactNode;
    right?: React.ReactNode;
}

export default class extends React.Component<IProps, {}> {
    public render() {
        const leftColumnClass = "left col-md-3 d-flex flex-row-reverse hidden-sm-down text-right pl-0";
        const centerColumnClass = "right col-md-9";
        const {left, right, ...rawProps} = this.props;
        const className = rawProps.className ? `row ${rawProps.className}` : "row";
        const props = {...rawProps, className};
        return (
            <section {...props}>
                <div className={leftColumnClass}>
                    {left}
                </div>
                <div className={centerColumnClass}>
                    {right}
                </div>
            </section>
        );
    }
}
