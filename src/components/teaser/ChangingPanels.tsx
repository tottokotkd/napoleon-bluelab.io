/**
 * Created by tottokotkd on 17/05/30.
 */
import {flatten, flow, head, shuffle} from "lodash";
import * as React from "react";
import Helmet from "react-helmet";
import * as ReactRouter from "react-router";

import { assetLink, prefixLink } from "../../utils/path-helper";
import Panels from "./Panels";

interface IProps extends ReactRouter.RouteComponentProps<undefined, undefined> {
    showMenu: boolean;
}
enum PanelContent {
    FirstTime,
    None,
    Before,
    After,
}
interface IData  {
    before: string;
    after: string;
}
interface IState {
    content: PanelContent;
    data: IData;
    timeouts: number[];
    intervals: number[];
}
export default class ChangingPanels extends React.Component<IProps, IState> {

    public static getRandomContent: () => IData = () => {
        const random = (count: number) => Math.floor(Math.random() * count);
        const result: string[][] = [];

        // 1.
        const r1 = random(3);
        result[0] = [
            Panels.allNames[r1 * 2],
            Panels.allNames[r1 * 2 + 1],
        ];

        // 2.
        const r2 = random(3) + 3;
        result[1] = [
            Panels.allNames[r2 * 2],
            Panels.allNames[r2 * 2 + 1],
        ];

        // 3.
        const r3 = random(3) + 6;
        result[2] = [
            Panels.allNames[r3 * 2],
            Panels.allNames[r3 * 2 + 1],
        ];

        // 4.
        const r4 = random(4) + 9;
        result[3] = [
            Panels.allNames[r4 * 2],
            Panels.allNames[r4 * 2 + 1],
        ];

        // 5.
        const r5 = random(1) + 13;
        result[4] = [
            Panels.allNames[r5 * 2],
            Panels.allNames[r5 * 2 + 1],
        ];

        const selected = flow(...[shuffle, head, flatten])(result);
        return {before: selected[0], after: selected[1]};
    }

    public state: IState = {
        content: PanelContent.FirstTime,
        data: ChangingPanels.getRandomContent(),
        intervals: [],
        timeouts: [],
    };

    public componentDidMount() {
        this.register();
    }

    public componentWillUnmount() {
        this.remove();
    }

    public render() {
        const {history, showMenu} = this.props;
        const shareTarget = this.state.content === PanelContent.After ? this.state.data.after : this.state.data.before;
        return (
            <Panels
                panelState={this.contentString()}
                showButton={false}
                showMenu={showMenu}
                onShareClick={`/teaser/${shareTarget}/`}
            >
                <Helmet meta={[{name: "twitter:image", content: assetLink("/img/twitter_card.png") }]}/>
                {this.props.children}
            </Panels>
        );
    }

    private change = () => {
        switch (this.state.content) {
            case PanelContent.FirstTime:
            case PanelContent.None:
                this.setState({content: PanelContent.Before});
                break;
            case PanelContent.Before:
                this.setState({content: PanelContent.After});
                break;
            case PanelContent.After:
                this.setState({content: PanelContent.None});
                break;
        }
    }

    private contentString = () => {
        switch (this.state.content) {
            case PanelContent.FirstTime:
                return "position-a";
            case PanelContent.None:
                return "position-a blue";
            case PanelContent.Before:
                return "position-b wave " + this.state.data.before;
            case PanelContent.After:
                return "position-b wave " + this.state.data.after;
        }
    }

    private register = () => {
        this.remove();

        const timeouts: number[] = [];

        timeouts.push(window.setTimeout(() => {
            this.randomize();
            this.change();
            const i = window.setInterval(() => {
                this.randomize();
                this.change();
            }, 12000);
            this.addInterval(i);
        }, 0));

        timeouts.push(window.setTimeout(() => {
            this.change();
            const i = window.setInterval(this.change, 12000);
            this.addInterval(i);
        }, 3000));

        timeouts.push(window.setTimeout(() => {
            this.change();
            const i = window.setInterval(this.change, 12000);
            this.addInterval(i);
        }, 6000));

        this.setState({timeouts});
    };

    private randomize() {
        this.setState(
            {data: ChangingPanels.getRandomContent(),
        });
    }

    private addInterval(i: number) {
        this.setState({
            intervals: this.state.intervals.concat(i),
        });
    }

    private remove = () => {
        for (const i of this.state.intervals) {
            window.clearInterval(i);
        }
        for (const t of this.state.timeouts) {
            window.clearTimeout(t);
        }
    }
}
