/**
 * Created by tottokotkd on 2017/07/08.
 */

import Link from "gatsby-link";
import * as React from "react";

export default class extends React.Component<{to: string, active?: boolean, disabled?: boolean}, {}> {
    public render() {
        const {to, active, disabled, children} = this.props;
        if (active) {
            return (
                <li className="nav-item active">
                    <Link className="nav-link" to={to}>
                        {children}
                        <span className="sr-only">(current)</span>
                    </Link>
                </li>
            );
        } else if (disabled) {
            return (
                <li className="nav-item">
                    <Link className="nav-link disabled" to={to} onClick={this.disableClick}>
                        {this.props.children}
                    </Link>
                </li>
            );
        } else {
            return (
                <li className="nav-item">
                    <Link className="nav-link" to={to}>
                        {this.props.children}
                    </Link>
                </li>
            );
        }
    }

    private disableClick(e: any) {
        e.preventDefault();
    }
}
