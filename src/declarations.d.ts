declare const graphql: (query: TemplateStringsArray) => void;

declare module "gatsby-config" {
    const siteMetadata: {
        title: string;
        url: string;
        assetServer: string;
    };
    const pathPrefix: string;
    const buildTime: string;
}
