import * as React from "react";
import Helmet from "react-helmet";

import {assetLink} from "../utils/path-helper";
import * as config from "../../gatsby-config.js";

import "prismjs/themes/prism-solarizedlight.css";

export default class DefaultLayout extends React.Component {
    render() {
        const {title, description} = config.siteMetadata;
        return (
            <div>
                <Helmet defaultTitle={title}

                    titleTemplate={`%s | ${title}`}
                    meta={[
                    { name: "description", content: description },
                    { name: "keywords", content: "Blue Napoleon" },
                    { name: "twitter:card", content: "summary" },
                    { name: "twitter:site", content: "@seymour_glasses" },
                    { name: "twitter:title", content: title },
                    { name: "twitter:description", content: description },
                    { name: "twitter:image", content: assetLink("/img/twitter_card.png")},
                    ]}
                >

                    {/* jQuery */}
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" />

                    {/* bootstrap */}
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
                    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" />
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" />

                    {/* font awesome */}
                    <script src="https://use.fontawesome.com/d71d176638.js" />

                    {/* favicon */}
                    <link rel="shortcut icon" href="/favicon.ico" />
                </Helmet>
                {this.props.children()}
            </div>
        );
    }
}
