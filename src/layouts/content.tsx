/**
 * Created by tottokotkd on 17/05/30.
 */

import Link from "gatsby-link";
import * as React from "react";
import MenuElement from "../components/layout/MenuElement";

import "../css/layout/content.scss";

export enum MenuItem {
    none,
    news,
    blog,
    about,
}

export default class Template extends React.Component<{item: MenuItem}, {}> {

    public render() {
        const {item} = this.props;

        const menuElements = [
            <MenuElement
                key={1}
                to="/news/"
                active={item === MenuItem.news}>news</MenuElement>,
            <MenuElement
                key={2}
                to="#"
                disabled>profile</MenuElement>,
            <MenuElement
                key={3}
                to="/blog/"
                active={item === MenuItem.blog}>blog</MenuElement>,
            <MenuElement
                key={4}
                to="/about/"
                active={item === MenuItem.about}>about</MenuElement>,
        ];

        return (
            <div id="content-layout">
                <header>
                    <div className="container">
                        <div className="row hidden-sm-down">
                            <div className="col-md-3 px-0 ">
                                <div className="navbar navbar-toggleable-sm navbar-inverse d-flex flex-row-reverse">
                                    <Link className="navbar-brand mx-0" to="/">napoleon.blue</Link>
                                </div>
                            </div>
                            <div className="col-md-9 px-0">
                                <div className="navbar navbar-toggleable-sm navbar-inverse">
                                    <nav className="collapse navbar-collapse">
                                        <ul className="navbar-nav mr-auto mt-3 mt-md-0">
                                            {menuElements}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="row hidden-md-up">
                            <div className="col-md-12 px-0">
                                <div className="navbar navbar-toggleable-sm navbar-inverse">
                                    <button
                                        type="button"
                                        className="navbar-toggler navbar-toggler-right"
                                        data-toggle="collapse"
                                        data-target="#navbarTogglerTarget"
                                        aria-controls="navbarTogglerTarget"
                                        aria-expanded="false"
                                        aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon" />
                                    </button>

                                    <Link className="navbar-brand mx-0" to="/">napoleon.blue</Link>

                                    <nav className="collapse navbar-collapse" id="navbarTogglerTarget">
                                        <ul className="navbar-nav mr-auto mt-2 mt-md-0">
                                            {menuElements}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                </header>

                {this.props.children}
            </div>
        );
    }
}
