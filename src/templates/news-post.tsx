/**
 * Created by tottokotkd on 2017/06/18.
 */

import * as React from "react";
import Layout, {MenuItem} from "../layouts/content";

import NewsPost, {IProps as NewsPostProps} from "../components/article/Post";
import PageRow from "../components/Row";

export default class extends React.Component<IProps, {}> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {ad, post} = this.props.data;
        return (
            <Layout item={MenuItem.news}>
                <NewsPost {...post}>
                    <PageRow className="ad" right={
                        <div dangerouslySetInnerHTML={{ __html: ad.html }} />
                    }/>
                </NewsPost>
            </Layout>
        );
    }
}

interface IProps {
    data: {
        post: NewsPostProps;
        ad: { html: string };
    };
}

export const pageQuery = graphql`
query NewsPostBySlug($slug: String!) {
  post: markdownRemark(fields: {slug: {eq: $slug}}) {
    html
    excerpt
    headings {
      value
      depth
    }
    frontmatter {
      title
      tags
      desc
      createdDate
      updatedDate
      icon {
        sharp: childImageSharp {
          data: responsiveResolution(width: 80, height: 80, quality: 100) {
            src
            srcSet
          }
        }
      }
      author {
        id
        name
        bio
        twitter
        avatar {
          sharp: childImageSharp {
            data: responsiveResolution(width: 80, height: 80, quality: 100) {
              src
              srcSet
            }
          }
        }
        icon {
          sharp: childImageSharp {
            data: responsiveResolution(width: 80, height: 80, quality: 100) {
              src
              srcSet
            }
          }
        }
      }
    }
  }

  ad: markdownRemark(fields: {slug: {eq: "/blog/ad.md/"}}) {
    html
  }
}
`;
