/**
 * Created by tottokotkd on 2017/06/18.
 */

import * as React from "react";
import Layout, {MenuItem} from "../layouts/content";

import AboutPost, {IProps as BlogPostProps} from "../components/article/Post";

export default class extends React.Component<IProps, {}> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {post} = this.props.data;
        return (
            <Layout item={MenuItem.about}>
                <AboutPost {...post} />
            </Layout>
        );
    }
}

interface IProps {
    data: { post: BlogPostProps; };
}

export const pageQuery = graphql`
query AboutPageBySlug($slug: String!) {
  post: markdownRemark(fields: {slug: {eq: $slug}}) {
    html
    excerpt
    headings {
      value
      depth
    }
    frontmatter {
      title
      tags
      desc
      createdDate
      updatedDate
      icon {
        sharp: childImageSharp {
          data: responsiveResolution(width: 80, height: 80, quality: 100) {
            src
            srcSet
          }
        }
      }
      author {
        id
        name
        bio
        twitter
        avatar {
          sharp: childImageSharp {
            data: responsiveResolution(width: 80, height: 80, quality: 100) {
              src
              srcSet
            }
          }
        }
        icon {
          sharp: childImageSharp {
            data: responsiveResolution(width: 80, height: 80, quality: 100) {
              src
              srcSet
            }
          }
        }
      }
    }
  }
}
`;
