/**
 * Created by tottokotkd on 2017/06/18.
 */

import * as React from "react";

import PostsList from "../components/article/List";
import {IProps as BlogProps} from "../components/article/ListItem";
import {MenuItem} from "../layouts/content";

export default class BlogPostTemplate extends React.Component<IProps, undefined> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {totalCount, posts} = this.props.data.result;
        const {tag, skip} = this.props.pathContext;
        return <PostsList menu={MenuItem.none} title={`${tag}に関係する記事`} posts={posts.map((p) => p.node)} />;
    }
}

interface IProps {
    data: { result: {
        totalCount: number;
        posts: [{node: BlogProps}];
    } };
    pathContext: {
        tag: string;
        skip: number;
    };
}

export const pageQuery = graphql`
query PostsByTag($tag: String!, $skip: Int!) {
  result: allMarkdownRemark(
    limit: 10
    skip: $skip
    sort: { fields: [frontmatter___createdDate], order: DESC }
    filter: {
      frontmatter: {
        tags: { in: [$tag] }
        draft: { ne: true }
      }
    }
  ) {
    totalCount
    posts: edges {
      node {
        excerpt
        timeToRead
        fields {
          slug
        }
        frontmatter {
          title
          tags
          desc
          createdDate
          updatedDate
          icon {
            sharp: childImageSharp {
              data: responsiveResolution(width: 80, height: 80, quality: 100) {
                src
                srcSet
              }
            }
          }
          author {
            icon {
              sharp: childImageSharp {
                data: responsiveResolution(width: 80, height: 80, quality: 100) {
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
}
`;
