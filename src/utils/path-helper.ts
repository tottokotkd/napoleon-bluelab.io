/**
 * Created by tottokotkd on 17/05/30.
 */

import {kebabCase} from "lodash";

export function prefixLink(relativePath: string): string  {
    const config = require("../../gatsby-config.js");
    const {pathPrefix} = config;
    if (pathPrefix) {
        return pathPrefix + relativePath;
    } else {
        return relativePath;
    }
}

export function assetLink(relativePath: string): string  {
    const config = require("../../gatsby-config.js");
    const path = config.siteMetadata.assetServer;
    return path + relativePath + `?t=${config.buildTime}`;
}

export function tagLink(tag: string): string {
    switch (tag) {
        case "news":
            return "/news/";

        default:
            return `/tags/${kebabCase(tag)}/`;
    }
}
