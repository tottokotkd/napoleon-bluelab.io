/**
 * Created by tottokotkd on 27/05/2017.
 */

import * as React from "react";
import * as ReactRouter from "react-router";
import StoppedPanels from "../../components/teaser/StoppedPanels";

export default class extends React.Component<ReactRouter.RouteComponentProps<undefined, undefined>, undefined> {
    public render() {
        return <StoppedPanels target="s4-3-1" history={this.props.history} route={this.props.route} />;
    }
}
