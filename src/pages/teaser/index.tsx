/**
 * Created by tottokotkd on 27/05/2017.
 */

import * as React from "react";
import * as ReactRouter from "react-router";

import Panels from "../../components/teaser/ChangingPanels";

export default class Index extends React.Component<ReactRouter.RouteComponentProps<undefined, undefined>, undefined> {
    public render() {
        return <Panels history={this.props.history} showMenu={true} />;
    }
}
